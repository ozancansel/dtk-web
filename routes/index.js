var express = require('express');
var router = express.Router();
var varsayilanRepo = require('../repo/varsayilan-repo');
var co = require('co');
var passport = require('passport');
var Gunluk = require('../model/sistemgunlugu');
var GunlukEnum = require('../model/enum/sistemgunluguenum');

module.exports = function (app) {

    app.use('/', router);

    /* GET home page. */
    router.get('/', function (req, res) {
        if (!req.isAuthenticated()) {
            res.redirect("/giris");
            return;
        }
        res.render('index', { title: 'Express', isim: req.user.isim });
    });

    router.get('/giris', function (req, res, next) {
        if (req.isAuthenticated())
            res.redirect('/');
        res.render('giris');
    });

    router.get('/cikis', function (req, res, next) {
        co(function*(){
            var kayit = new Gunluk({
                aksiyon : GunlukEnum.Aksiyonlar.Cikis ,
                tip : GunlukEnum.Tipler.Eylem ,
                aktorler : req.user._id ,
                tarih : Date.now()
            });

            try{
                yield kayit.save();
            }catch(e){
                //Hata olustuysa pas geciliyor
            }
            req.logout();
            res.redirect('/giris');
        });
    });

    router.post('/giris', passport.authenticate('local'), function (req, res) {
        co(function* () {
            //Kayit yaziliyor
            var kayit = new Gunluk({
                aksiyon: GunlukEnum.Aksiyonlar.Giris ,
                tip: GunlukEnum.Tipler.Eylem ,
                veri : { }, 
                aktorler: [ req.user._id ] ,
                tarih: Date.now()
            });

            try{
                yield kayit.save();
            }catch(e){      
            }
        });

        res.redirect('/');
    });

    router.get('/error', function (req, res) {
        res.status(400);
        res.render('error', { message: "Hata olustu " + res.statusCode });
    });
}