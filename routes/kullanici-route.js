var express = require('express');
var router = express.Router();
var yetkilendirme = require('../authority/kullanici-yetkilendirme');
var Kullanici = require('../model/kullanici');
var Gunluk = require('../model/sistemgunlugu');
var GunlukEnum = require('../model/enum/sistemgunluguenum');
var KullaniciRepo = require('../repo/kullanici-repo');
var Roller = require('../model/yetkiler');
var Ebeveyn = require('../model/ebeveyn');
var co = require('co');
var Joi = require('joi');
var logger = require('../logger');
var Enums = require('../enum');
const url = require('url');

const kullaniciEkleSchema = Joi.object({
  isim: Joi.string().min(2).required(),
  sifre: Joi.string().required(),
  telefon: Joi.string().regex(/\(?([0-9]{3})\)?([ .-]?)([0-9]{3})\2([0-9]{4})/),
  email: Joi.string().email(),
  rol: Joi.number().min(Roller.Admin).max(Roller.Veli).required()
}).unknown();

const kullaniciSilSchema = Joi.object().keys({
  yonlendir: Joi.string().required(),
  silinecekler: Joi.array().required()
});

const veliAtaGetSchema = Joi.object({
  id: Joi.string().length(24).required()
}).unknown();

const veliAtaPostSchema = Joi.object({
  veliId: Joi.string().length(24).required(),
  ogrenciId: Joi.string().length(24).required()
});

module.exports = function (app) {

  app.use('/kullanici', router);

  /* GET users listing. */

  router.get('/kullaniciEkle', yetkilendirme.can('kontrol'),
    function (req, res, next) {
      res.render('kullanici/kullaniciEkle');
    });

  router.post('/kullaniciEkle', yetkilendirme.can('kontrol'),
    function (req, res) {
      const { error, value } = Joi.validate(req.body, kullaniciEkleSchema);

      //Eger body dogru sekilde gonderilmemisse
      if (error !== null) {
        res.status(400);
        res.render('error', { mesaj: "Hatali model." });
        return;
      }

      co(function* () {
        var mevcut = yield Kullanici.findOne({ isim: req.body.isim }).exec();

        //Eger kullanici mevcutsa
        if (mevcut) {
          res.status(409);
          res.render('kullanici/kullaniciEkle', { mesaj: "Bu isimli kullanici mevcut" });
          return;
        }

        var kull = new Kullanici({
          isim: req.body.isim,
          sifre: req.body.sifre,
          rol: req.body.rol,
          telefon: req.body.telefon,
          email: req.body.email,
          kayitTarihi: new Date(Date.now())
        });

        //Kullanici ekleniyor
        var eklenenKullanici = yield kull.save();
        var ekleyenKullanici = req.user;

        var kayit = new Gunluk({
          aksiyon: GunlukEnum.Aksiyonlar.KullaniciEkle,
          tip: GunlukEnum.Tipler.Eylem,
          aktorler: [eklenenKullanici._id, ekleyenKullanici._id],
          tarih: Date.now()
        });

        //Log veritabanina yaziliyor
        try {
          yield logger.dbInfoLog(eklenenKullanici.isim + ' eklendi.', kayit);
        } catch (err) {
          //Log pas geciliyor
        }

        res.status(200);
        res.render('kullanici/kullaniciEkle', { mesaj: "Kullanici basariyla eklendi.", sonuc: Enums.FormSonuc.Success });
      }).catch(function (err) {
        console.log(err);
        res.status(500);
        res.render('error', { mesaj: 'Kullanici eklenirken hata olustu.' });
      });
    });

  router.get('/listele', yetkilendirme.can('kontrol'),
    function (req, res) {
      co(function* () {

        var filter = {
          _id: { $ne: req.user._id }
        };

        if ('isim' in req.query)
          filter.isim = {
            $regex: req.query.isim
          }

        //Ogretmenler adminleri goremiyor
        if (req.user.rol === Roller.Ogretmen) {
          filter = {
            rol: { $ne: Roller.Admin }
          }
        }

        var kullanicilar = yield Kullanici.find(filter).exec();
        var kirpilmis = kullanicilar.map(function (kull) {
          var nesne = {
            isim: kull.isim,
            email: kull.email,
            telefon: kull.telefon,
            rol: kull.rol,
            id: kull._id
          };
          return nesne;
        });

        res.render('kullanici/listele', { kullanicilar: kirpilmis });
      }).catch(function (err) {
        logger.error('GET /kullanici/listele', 'Hata => ' + err);
      });
    });

  router.post('/kullaniciSil', yetkilendirme.can('kontrol'),
    function (req, res) {

      const { error, value } = Joi.validate(req.body, kullaniciSilSchema);

      if (error !== null) {
        res.status(400);
        res.render('error', { mesaj: 'Hatali model' });
        return;
      }

      if (req.body.silinecekler.length === 0) {
        res.status(400);
        res.render('error', { mesaj: 'Silinecek kullanicilar belirtilmemis.' });
      }

      for (var i = 0; i < req.body.silinecekler.length; i++) {
        if (req.user._id == req.body.silinecekler[i]) {
          res.status(409);
          res.render('error', { mesaj: 'Silinecekler listesinde kullanicinin kendisi de mevcut' });
          return;
        }
      }

      co(function* () {

        for (var i = 0; i < req.body.silinecekler.length; i++) {
          var id = req.body.silinecekler[i];

          var kull = yield Kullanici.findOne({ _id: id }).exec();

          //Kullanici mevcut degilse
          if (kull === null) {
            res.status(400);
            res.render('error', { mesaj: 'Silinmek istenen kullanici mevcut degil.' });
            return;
          }

          //siliniyor
          yield Kullanici.remove(kull);

          yield logger.dbInfoLog(kull.isim + ' silindi', new Gunluk({
            aksiyon: GunlukEnum.Aksiyonlar.KullaniciSilme,
            tip: GunlukEnum.Tipler.Eylem,
            aktorler: [req.user._id, kull._id],
            tarih: Date.now()
          }));
        }

        //Istek yonlendiriliyor
        res.redirect(req.body.yonlendir);
      }).catch(function (err) {
        co(function* () {
          var kayit = new Gunluk({
            aksiyon: GunlukEnum.Aksiyonlar.KullaniciSilme,
            tip: GunlukEnum.Tipler.Hata,
            veri: { silinecekler: req.body.silinecekler, }
          });

          try {
            yield logger.dbErrorLog(kull.isim + ' silinirken hata olustu');
          } catch (e) {
            //Hata olusursa gormezden geliniyor
          }
          res.status(500);
          res.render('error', { mesaj: 'Islemler sirasinda hata olustu.' });
        }).catch(function (err) {
        });
      });
    });

  router.get('/veliAta', yetkilendirme.can('kontrol'), function (req, res) {

    const { error, value } = Joi.validate(req.query, veliAtaGetSchema);

    //Eger body dogru sekilde gonderilmemisse
    if (error !== null) {
      res.status(400);
      res.render('error', { mesaj: "Hatali model." });
      return;
    }

    co(function* () {

      var kullanici = yield Kullanici.findOne({ _id: req.query.id }).exec();

      if (kullanici === null) {
        res.status(400);
        res.render('error', { mesaj: 'Kullanici bulunamadi.' })
        return;
      }

      //Veli atanmak istenen kullanicinin yetkisi kontrol ediliyor
      if (kullanici.rol != Roller.Ogrenci) {
        logger.error("/kullanici/veliAta ogrenci harici veli atanmaya calisiliyor", req.user._id + " idli kullanici " + kullanici._id + ' li kullaniciya veli atamak istedi.');

        //Hata sayfasina yonlendiriliyor
        res.status(400);
        res.render('error', { mesaj: "Sadece ogrencilere veli atanabilir." });

        return;
      }

      //Veliler veritabanindan aliniyor
      var veliler = yield Kullanici.find({ rol: Roller.Veli }).exec();

      var velilerDuzenlenmis = veliler.map(function (veli) {
        var duzenlenmis = {
          id: veli._id,
          isim: veli.isim
        };

        return duzenlenmis;
      });

      if(req.query.sonuc !== undefined && parseInt(req.query.sonuc) == Enums.FormSonuc.Error)
        res.status(500);

      //Sayfa yukleniyor
      res.render('kullanici/veliAta', { ogrenciId: kullanici._id, isim: kullanici.isim, veliler: velilerDuzenlenmis });
    }).catch(function (err) {
      logger.error('GET /kullanici/veliAta bilinmeyen bir hata olustu hata olustu.', err);
      res.status(400);
      res.render('error', { mesaj: 'Bilinmeyen bir hata olustu' });
    });
  });

  router.post('/veliAta', yetkilendirme.can('kontrol'), function (req, res) {

    const { error, value } = Joi.validate(req.body, veliAtaPostSchema);

    if (error !== null) {
      res.status(400);
      res.render('error', { mesaj: 'Model hatali ' });
      return;
    }

    co(function* () {
      //Ogrenci veritabanindan okunuyor
      var ogrenci = yield Kullanici.findOne({ _id: req.body.ogrenciId }).exec();

      //Ogrenci mevcut degilse
      if (ogrenci === null) {
        res.status(400);
        res.render('error', { mesaj: 'Belirtilen ogrenci mevcut degil.' });
        return;
      }

      //Rol kontrol ediliyor
      if (ogrenci.rol !== Roller.Ogrenci) {
        res.status(400);
        res.render('error', { mesaj: 'Ogrenci olarak belirtilen kullanici ogrenci degil.' });
        return;
      }

      //Veli veritabanindan okunuyor
      var veli = yield Kullanici.findOne({ _id: req.body.veliId }).exec();

      //Veli mevcut degilse
      if (veli === null) {
        res.status(400);
        res.render('error', { mesaj: 'Belirtilen veli mevcut degil.' });
        return;
      }

      //Rol kontrol ediliyor
      if (veli.rol !== Roller.Veli) {
        res.status(400);
        res.render('error', { mesaj: 'Veli olarak belirtilen kullanici veli degil.' });
        return;
      }

      //Entity olusturuluyor
      var ebeveyn = new Ebeveyn({
        veliId: veli._id,
        ogrenciId: ogrenci._id
      });

      try {
        yield ebeveyn.save();
      } catch (err) {
        yield logger.dbErrorLog('Ebeveyn kaydedilirken hata olustu.',
          new Error().stack,
          new Gunluk({
            aksiyon: GunlukEnum.Aksiyonlar.VeliAta,
            tip: GunlukEnum.Tipler.Hata,
            veri: {
              ogrenciId: ogrenci._id,
              veliId: veli._id,
              msg: err
            },
            aktorler: [req.user._id]
          }));

          res.redirect(url.format({
            pathname : '/veliAta' ,
            query : {
              'id' : ogrenci._id ,
              'sonuc' : Enums.FormSonuc.Error ,
              'mesaj' : 'Ebeveyn kaydedilirken hata olustu.'
            }
          }));

          return;
      }

      try{
        yield logger.dbInfoLog('Ebeveyn basariyla yazildi. Ogrenci => ' + ogrenci._id + ' Veli => ' + veli._id , 
                    new Gunluk({
                      aksiyon : GunlukEnum.Aksiyonlar.VeliAta , 
                      tip : GunlukEnum.Tipler.Eylem ,
                      veri : { 
                        ogrenciId : ogrenci._id , 
                        veliId : veli._id
                      } ,
                      aktorler : [ req.user._id ] ,
                    }));
        } catch(err){

        }

        res.redirect(url.format({
          pathname : '/veliAta' ,
          query : {
            'id' : ogrenci._id ,
            'sonuc' : Enums.FormSonuc.Success ,
            'mesaj' : 'Ebeveyn basariyla eklendi.'
          }
        }));
    }).catch(function(err){
      logger.error('POST /kullanici/veliAta icerisinde bilinmeyen hata olustu.' , err);

      res.status(400);
      res.render('error' , { mesaj : 'Hata olustu!'});
    });
  });
}