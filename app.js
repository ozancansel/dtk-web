var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var lessMiddleware = require('less-middleware');
var mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
var config = require('./config');
var indexRoute = require('./routes/index');
var kullaniciRoute = require('./routes/kullanici-route');
var passport = require('passport');
var auth = require('./authentication');
var session = require('express-session');
var kullaniciYetkilendirme = require('./authority/kullanici-yetkilendirme');
var varsayilanRepo = require('./repo/varsayilan-repo');
var cmdParser = require('minimist');
var parsedArgs = cmdParser(process.argv.slice(2));
var logger = require('./logger');
var co = require('co');
var enums = require('./enum');


var app = express();
var dbName = config.database;


//Eger test calistiriliyorsa, uzerinde calisilan veritabani test veritabani olarak degistiriliyor.
if('test' in parsedArgs){
    dbName = config.test;
    varsayilanRepo.initTestDb();
} else {
    varsayilanRepo.init();
}

// view engine setup
mongoose.connect(dbName);
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.use(function(req,res,next){
    res.locals.user = req.user;
    next();
});
app.locals = {
    global : {
        Roller : require('./model/yetkiler')
    } ,
    enums : enums
};

app.testLocals = {
    db  : dbName
};

//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
app.use(cookieParser());
app.use(lessMiddleware(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));
app.use(session({   secret  :   "k$D^{Zb=KMc4~tn*oEN*KFL}'JHV^K'c9QBf1tkHq:O?EaqG]5P=TQZNKtO%KxY"}))
app.use(passport.initialize());
app.use(passport.session());
app.use(kullaniciYetkilendirme.middleware());

indexRoute(app);
kullaniciRoute(app);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    logger.error('Hata' , err.message);

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

logger.info('Sunucu baslatildi.');

module.exports = app;