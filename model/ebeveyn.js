
var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var co = require('co');

var EbeveynSema = new Schema({
    veliId : { type : Schema.ObjectId , required : true , ref : 'Kullanici' } ,
    ogrenciId : { type : Schema.ObjectId , required : true , ref : 'Kullanici' } ,
    tarih : { type : Date , required : true , default : Date.now() }
});

module.exports = mongoose.model('Ebeveyn' , EbeveynSema);

module.exports.ensureIndexes(function(err){
    if(err)
        console.log(err);
});