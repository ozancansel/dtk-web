/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

module.exports = {
    Admin : 0 ,
    Ogretmen : 1 ,
    Ogrenci : 2 , 
    Veli : 3 ,
    getStr : function(rol){
        var str;
        switch(rol){
            case this.Admin : str = 'Admin'; break;
            case this.Ogretmen : str = 'Ogretmen';   break;
            case this.Ogrenci : str = 'Ogrenci'; break;
            case this.Veli : str =  'Veli'; break; 
            default :   str = ''; break;
        }

        return str;
    }
};
