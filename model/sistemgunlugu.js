var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var SistemGunlugu = new Schema({
    aksiyon : { type : Number , required : true } ,
    tip : { type : Number , required : true } ,
    veri : { }, 
    aktorler : [Schema.ObjectId] , //Bu kayitla ilgisi olan kullanicilarin listesi
    tarih : { type : Date , required : true }
});

//Modeldeki veriler aksiyona gore farkliliklar gosterecegi icin ActionLog.veri kisminda tip belirtilmemistir.
//Bu alan aksiyona gore doldurulup, aksiyona gore okunur. Aksiyon ise 'tip' uzerinden belirlenir.
//Yani ornegin 
// { aksiyon : 1 , veri : { ekleyenKisiId : 5 } , tarih : 12/02/2017 } seklinde bir veri var ise
// aksiyon 1'e karsilik gelen aksiyona gore veri kismi islenir.
// Bu veri polimorfik bir yapiyla islenecektir.

module.exports = mongoose.model('SistemGunlugu' , SistemGunlugu);

module.exports.ensureIndexes(function (err){
    if(err){
        console.log(err);
    }
});