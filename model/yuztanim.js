var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var YuzTanimSema = new Schema({
    aitOlduguKullanici : { type : ObjectId , required : true , ref : 'Kullanici'} , //Yuzun ait oldugu kullanici
    metrikTanim : [Number], //Yuzun metrik tanimlari bu dizide tutulur
    resim : { type : String , required : true } //Resim dizin
});

//Her kullanici icin sadece bir tane kayit bulunmalidir.
YuzTanimSema.index({ aitOlduguKullanici : 1 });

//Metriktanim dizisi double tipindeki sayilardan olusur. Dizi 128 eleman icerir.
//Ornek
/* 
    {
        aitOlduguKullanici : _1934324912931 ,
        metrikTanim : [ 0.7332 , 0.34222 , .... , 0.232 ] ,
        resim : "./023412.jpg"
    }
* 
*/

module.exports = mongoose.model('YuzTanim' , YuzTanimSema);

module.exports.ensureIndexes(function(err){
    if(err)
        console.log(err);
});