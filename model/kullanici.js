/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var mongoose = require('mongoose');
var Ebeveyn = require('./ebeveyn');
var Schema = mongoose.Schema;

var KullaniciSema = new Schema({
   isim     :   { type : String , required : true , index : { unique : true } } ,
   email    :   { type : String } ,
   telefon  :   { type : String } ,
   sifre    :   { type : String , required : true  } ,
   rol      :   { type : Number , required : true  } ,
   kayitTarihi  :   { type : Date , required : true , default : Date.now()}
});

module.exports = mongoose.model('Kullanici' , KullaniciSema);


KullaniciSema.post('remove' , function(doc){
    co(function*(){
        var ebeveynIliskiler = yield Ebeveyn.find({ $or : [ { veliId : doc._id } , { ogrenciId : doc._id } ] }).exec();

        for(var i = 0; i < ebeveynIliskiler.length; i++){
            yield ebeveynIliskiler[i].remove();
        }
    });
});

module.exports.ensureIndexes(function (err) {
    if (err) {
        console.log(err);
    }
});
