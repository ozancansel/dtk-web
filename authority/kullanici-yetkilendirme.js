/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var ConnectRoles = require('connect-roles');
var Yetkiler = require('../model/yetkiler');

var kullanici = new ConnectRoles({
  failureHandler: function (req, res, action) {
    // optional function to customise code that runs when 
    // user fails authorisation 
    var accept = req.headers.accept || '';
    res.status(403);
    if (~accept.indexOf('html')) {
      //res.render('/', {action: action});
      res.render('access-denied');
    } else {
      res.send('Access Denied - You don\'t have permission to: ' + action);
    }
  }
});

var izinler = [
  {
    'expression': /\/kullanici\/listele/,
    'GET': [Yetkiler.Admin, Yetkiler.Ogretmen],
    'POST': [Yetkiler.Admin, Yetkiler.Ogretmen]
  },
  {
    'expression': /\/kullanici\/kullaniciEkle/,
    'GET': [Yetkiler.Admin],
    'POST': [Yetkiler.Admin]
  } ,
  {
    'expression' : /\/kullanici\/kullaniciListele/ , 
    'GET' : [Yetkiler.Admin , Yetkiler.Ogretmen] ,
  } ,
  {
    'expression' : /\/kullanici\/kullaniciSil/ ,
    'POST' : [ Yetkiler.Admin ]
  } ,
  {
    'expression' : /\/kullanici\/veliAta/ ,
    'GET' : [ Yetkiler.Admin , Yetkiler.Ogretmen ]
  } ,
  {
    'expression' : /\/kullanici\/veliAta/ ,
    'POST' : [ Yetkiler.Admin , Yetkiler.Ogretmen ]
  }
];

kullanici.use(function (req, action) {
  if (!req.isAuthenticated()) return action === "access home page";
});

kullanici.use('admin', function (req) {
  if (req.user.rol === Yetkiler.Admin)
    return true;
});

kullanici.use('ogretmen', function (req) {
  if (req.user.rol === Yetkiler.Ogretmen)
    return true;
});

kullanici.use('ogrenci', function (req) {
  if (req.user.rol === Yetkiler.Ogrenci)
    return true;
});

kullanici.use('kullanici ekle', function (req) {
  if (req.user.rol === Yetkiler.Admin) {
    return true;
  }

  return false;
});

kullanici.use('kullanici/listele', function (req) {
  if (req.user.rol === Yetkiler.Ogretmen || req.user.rol === Yetkiler.Admin)
    return true;
});

kullanici.use('kontrol', function (req) {
  // if(req.originalUrl in izinler && izinler[req.originalUrl].indexOf(req.user.rol) !== -1){
  
  for(var i = 0; i < izinler.length; i++){
    var izin = izinler[i];
    //Eger ki eslesme saglandiysa
    if(req.originalUrl.match(izin.expression)){
      if(req.method in izin && izin[req.method].indexOf(req.user.rol) !== -1){
        return true;
      }
    }
  }

  return false;
});

kullanici.use(function (req) {
  if (req.user.rol === Yetkiler.Admin)
    return true;
});

module.exports = kullanici;