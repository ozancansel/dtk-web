var Kullanici = require('./model/kullanici');
var passport = require('passport')
        , LocalStrategy = require('passport-local').Strategy;

passport.use(new LocalStrategy({
        usernameField : "isim" ,
        passwordField : "sifre"
    } ,
    function(isim, sifre, done) {
        Kullanici.findOne({isim: isim}, function (err, user) {
            if (err) {
                return done(err);
            }
            if (!user) {
                return done(null, false, {message: 'Incorrect username.'});
            }
            if (user.sifre !== sifre) {
                return done(null, false, {message: 'Incorrect password.'});
            }
            return done(null, user);
        });
    }
));

passport.serializeUser(function (user, done) {
    done(null, user._doc._id);
});

passport.deserializeUser(function (id, done) {
    Kullanici.findOne({_id: id}, function (hata, kullanici) {
        done(hata, kullanici);
    });
});