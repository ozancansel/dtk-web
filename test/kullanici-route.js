'use strict'
var session = require('supertest-session');
var expect = require('chai').expect;
var Roller = require('../model/yetkiler');
var originalApp = require('../app');
var mongoose = require('mongoose');
var Kullanici = require('../model/kullanici');
var Gunluk = require('../model/sistemgunlugu');
var Roller = require('../model/yetkiler');
var GunlukEnum = require('../model/enum/sistemgunluguenum');
var Ebeveyn = require('../model/ebeveyn');
var Enums = require('../enum');
var co = require('co');
var async = require('async');
const url = require('url');
const cheerio = require('cheerio');

describe('GET /kullanici/kullaniciEkle', function () {
    var app, request;

    beforeEach(function () {
        app = originalApp;

        request = session(app);
    });

    it('kullanici giris yapmamissa 403 dondurmeli', function (done) {
        request
            .get('/kullanici/kullaniciEkle')
            .expect(403, done);
    });

    it('kullanici ogrenci olarak giris yapmissa 403 dondurmeli', function (done) {
        co(function* () {
            //Kullanicilar temizleniyor
            yield Kullanici.remove({});
            var kull = new Kullanici({
                isim: "ogrenci",
                sifre: "ogrenci",
                rol: Roller.Ogrenci
            });
            //Kullanici veritabanina yaziliyor
            yield kull.save();
            request
                .post('/giris')
                .type('json')
                .send({ isim: 'ogrenci', sifre: 'ogrenci' })
                .then(function (res) {
                    request.get('/kullanici/kullaniciEkle')
                        .expect(403, done);
                });
        });
    });

    it('kullanici ogretmen olarak giris yapmissa 403 dondurmeli', function (done) {
        co(function* () {
            yield Kullanici.remove({});
            var kull = new Kullanici({
                isim: "ogretmen",
                sifre: "ogretmen",
                rol: Roller.Ogretmen
            });
            yield kull.save();
            request
                .post('/giris')
                .type('/json')
                .send({ isim: 'ogretmen', sifre: 'ogretmen' })
                .then(function (res) {
                    request.get('/kullanici/kullaniciEkle')
                        .expect(403, done);
                });
        });
    });

    it('kullanici login olmus ve admin ise 200 dondurmeli', function (done) {
        co(function* () {
            //Kullanicilar temizleniyor
            yield Kullanici.remove({});
            var kull = new Kullanici({
                isim: "admin",
                sifre: "admin",
                rol: Roller.Admin
            });
            //Kullanici veritabanina yaziliyor
            yield kull.save();
            request
                .post('/giris')
                .type('json')
                .send({ isim: 'admin', sifre: 'admin' })
                .then(function (res) {
                    request.get('/kullanici/kullaniciEkle')
                        .expect(200, done);
                });
        });
    });

});

describe('POST kullanici/kullaniciEkle', function () {
    var app, request, route;

    beforeEach(function () {
        app = originalApp;

        request = session(app);
    });

    it('kullanici login olmamis ise 403 dondurmeli', function (done) {
        request
            .post('/kullanici/kullaniciEkle')
            .expect(403, done);
    });

    it('kullanici login olmus fakat admin degilse 403 dondurmeli', function (done) {
        co(function* () {
            yield Kullanici.remove({});//Kullanicilar temizleniyor
            var kull = new Kullanici({
                isim: 'ogrenci',
                sifre: 'ogrenci',
                rol: Roller.Ogrenci
            });
            var kull = yield kull.save();
            request
                .post('/giris')
                .type('json')
                .send({
                    isim: 'ogrenci',
                    sifre: 'ogrenci'
                })
                .then(function (res) {
                    request.post('/kullanici/kullaniciEkle')
                        .expect(403, done);
                });
        });
    });

    describe('Kullanici admin olarak giris yapmis ve', function (done) {
        var app, request, admin = {
            isim: 'admin',
            sifre: 'admin',
            rol: Roller.Admin
        };

        //Her testten once admin olarak login olunuyor
        beforeEach(function (done) {
            app = originalApp;

            request = session(app);

            request.post('/giris')
                .type('json')
                .send({ isim: 'admin', sifre: 'admin' })
                .then(function (res) {
                    done();
                })
        });

        //Suite baslamadan once admin tipinde kullanici olusturulup veritabanina ekleniyor
        before(function (done) {
            co(function* () {
                yield Kullanici.remove({});
                var kull = new Kullanici(admin);
                admin._id = kull._id;
                yield kull.save();
                done();
            });
        });

        //Suite bittikten sonra Kullanicilar temizleniyor
        after(function (done) {
            co(function* () {
                yield Kullanici.remove({});
                done();
            })
        });

        it('isim belirtilmemisse 400 dondurmeli', function (done) {
            request.
                post('/kullanici/kullaniciEkle')
                .type('json')
                .send({
                    sifre: '12345',
                    rol: Roller.Ogrenci,
                    telefon: '05347658765'
                })
                .expect(400, done);
        });

        it('sifre belirtilmemisse 400 dondurmeli', function (done) {
            request
                .post('/kullanici/kullaniciEkle')
                .type('json')
                .send({
                    isim: 'testuser',
                    rol: Roller.Ogrenci,
                    telefon: '530 000 00 00'
                })
                .expect(400, done);
        });

        it('yetki belirtilmemisse 400 dondurmeli', function (done) {
            request
                .post('/kullanici/kullaniciEkle')
                .type('json')
                .send({
                    isim: 'testuser',
                    sifre: 'testuser',
                    sifreTekrar: 'testuser',
                    telefon: '530 000 00 00'
                })
                .expect(400, done);
        });

        it('sifreler eslesmiyorsa 400 dondurmeli', function (done) {
            request
                .post('/kullanici/kullaniciEkle')
                .type('json')
                .send({
                    isim: 'testuser',
                    sifre: 'testuser',
                    sifreTekrar: 'testuser',
                    telefon: '530 000 00 00',
                })
                .expect(400, done);
        });

        var dogruKullanicilar = [
            { isim: 'user1', sifre: 'user1', rol: Roller.Ogrenci },
            { isim: 'user2', sifre: 'user2', rol: Roller.Ogrenci, telefon: '5340628572' },
            { isim: 'user3', sifre: 'user3', rol: Roller.Veli, email: 'veli@gmail.com' }
        ];

        dogruKullanicilar.forEach(function (kullanici) {
            it(kullanici.isim + '\'li kullanici eklenmeli ve 200 dondurmeli', function (done) {
                //Istek yollaniyor
                request
                    .post('/kullanici/kullaniciEkle')
                    .type('json')
                    .send(kullanici)
                    .expect(200)
                    .end(function (err, res) {
                        if (err)
                            throw err;
                        co(function* () {
                            //Kullanici veritabanindan kontrol ediliyor
                            var kull = yield Kullanici.find(kullanici).exec();
                            expect(kull).to.not.equal(null);
                            //Eklenen test kullanicisi siliniyor
                            yield Kullanici.remove({ _id: kull._id }).exec();
                            done();
                        });
                    });
            });
        });

        var hataliKullanicilar = [
            { body: { sifre: 'sifre', rol: Roller.Ogrenci }, aciklama: "isim belirtilmemis ise " },
            { body: { isim: 'isim', rol: Roller.Ogrenci }, aciklama: "sifre belirtilmemis ise " },
            { body: { isim: 'isim', sifre: 'sifre' }, aciklama: "yetki belirtilmemis ise " },
            { body: { isim: 'isim', sifre: 'sifre', rol: Roller.Ogrenci, telefon: '534211' }, aciklama: 'telefon yalnis girilmisse ' },
            { body: { isim: 'isim', sifre: 'sifre', rol: Roller.Ogrenci, email: "yanlismail" }, aciklama: 'mail yanlis girilmisse' },
            { body: { isim: 'isim', sifre: 'sifre', rol: 20 /*Mevcut olmayan yetki icin rastgele bir sayi*/ }, aciklama: "yetki yanlis girilmisse " }
        ];

        hataliKullanicilar.forEach(function (kull) {
            it(kull.aciklama + ' 400 dondurmeli', function (done) {
                request
                    .post('/kullanici/kullaniciEkle')
                    .type('json')
                    .send(kull.body)
                    .expect(400, done);
            });
        });

        it('kullanici daha onceden mevcutsa 409 dondurmeli', function (done) {
            co(function* () {
                var kull = {
                    isim: 'mevcut1',
                    sifre: 'mevcut1',
                    rol: Roller.Ogrenci
                };

                var saved = yield new Kullanici(kull).save();

                request
                    .post('/kullanici/kullaniciEkle')
                    .type('json')
                    .send(kull)
                    .expect(409)
                    .end(function (err, res) {
                        co(function* () {
                            yield Kullanici.remove(kull);
                            done();
                        });
                    });
            });
        });

        it('yeni kullanici eklenirse sistem gunlugune eklenmeli', function (done) {
            co(function* () {
                //Gunluk temizleniyor
                yield Gunluk.remove({});

                //Yeni kullanici olarak dogru kullanicilardan herhangi bir tanesi seciliyor
                var yeniKullanici = dogruKullanicilar[0];

                //Kullanicinin veritabaninda var olmadigindan emin olunuyor
                yield Kullanici.remove(yeniKullanici);

                request
                    .post('/kullanici/kullaniciEkle')
                    .type('json')
                    .send(yeniKullanici)
                    .expect(200)
                    .end(function (err, res) {
                        co(function* () {
                            var c = yield Gunluk.count().exec();
                            expect(c > 0).to.be.true;
                            var eklenenKullanici = yield Kullanici.findOne({ isim: yeniKullanici.isim }).exec();
                            var kayit = yield Gunluk.findOne({ aktorler: { $in: [admin._id, eklenenKullanici._id] } }).exec();
                            expect(kayit).to.not.be.null;
                            done();
                        });
                    });
            });
        });

        it('Eger kullanici eklenirse basarili mesaji dondurmeli', function (done) {
            co(function* () {
                var yeniKullanici = dogruKullanicilar[0];

                yield Kullanici.remove(yeniKullanici);

                request
                    .post('/kullanici/kullaniciEkle')
                    .type('json')
                    .send(yeniKullanici)
                    .expect(200)
                    .end(function (err, res) {
                        co(function* () {
                            var kullaniciSayisi = yield Kullanici.count().exec();
                            var $ = cheerio.load(res.text);
                            var rowCount = $('.alert.alert-success').length;
                            expect(rowCount).to.not.equal(0);// -1 cunku kullanici kendini gostermemeli
                            done();
                        }).catch(function (err) {
                            done(err);
                        });
                    });
            });
        });
    })
});



describe('POST /kullanici/kullaniciSil', function (done) {
    var request;

    beforeEach(function (done) {
        co(function* () {
            request = session(originalApp);
            //Kullanicilar temizleniyor
            yield Kullanici.remove().exec();
            done();
        });
    });

    it('Ogrencinin silme istegi reddediliyor', function (done) {

        var k = new Kullanici({
            isim: 'ogrenci',
            sifre: 'ogrenci',
            rol: Roller.Ogrenci
        });

        request
            .post('/giris')
            .type('json')
            .send({ isim: k.isim, sifre: k.sifre })
            .end(function (err, res) {
                request
                    .post('/kullanici/kullaniciSil')
                    .expect(403, done);
            });
    });

    describe('admin olarak giris yapilmissa => ', function () {

        var request;

        var admin, admin2, ogrenci, silinecekKullanici1, silinecekKullanici2;

        beforeEach(function (done) {
            co(function* () {
                request = session(originalApp);

                //Kullanicilar temizleniyor
                yield Kullanici.remove().exec();
                //Admin ve test kullanicilari ekleniyor
                admin = yield new Kullanici({
                    isim: 'admin1',
                    sifre: 'admin1',
                    rol: Roller.Admin
                }).save();
                admin2 = yield new Kullanici({
                    isim: 'admin2',
                    sifre: 'admin2',
                    rol: Roller.Admin
                }).save();
                ogrenci = yield new Kullanici({
                    isim: 'ogrenci',
                    sifre: 'ogrenci',
                    rol: Roller.Ogrenci
                }).save();
                silinecekKullanici1 = yield new Kullanici({
                    isim: 'silinecek',
                    sifre: 'silinecek',
                    rol: Roller.Ogrenci
                }).save();
                silinecekKullanici2 = yield new Kullanici({
                    isim: 'silinecek2',
                    sifre: 'silinecek2',
                    rol: Roller.Veli
                }).save();

                //Admin olarak sisteme giris yapiliyor
                request
                    .post('/giris')
                    .type('json')
                    .send({ isim: admin.isim, sifre: admin.sifre })
                    .expect(302)
                    .end(done);
            });
        });

        afterEach(function (done) {
            co(function* () {
                yield Kullanici.remove().exec();
                done();
            });
        });

        it('kullanici idsi gonderilmemisse 400 donduruluyor', function (done) {
            request
                .post('/kullanici/kullaniciSil')
                .type('json')
                .expect(400, done);
        });

        it('kullanici silinirse belirtilen urlye yonlendirilmeli', function (done) {
            request
                .post('/kullanici/kullaniciSil')
                .type('json')
                .send({ yonlendir: '/', silinecekler: [silinecekKullanici1._id] })
                .expect(302)
                .end(function (err, res) {
                    expect(res.headers['location']).to.equal('/');
                    done();
                });
        });

        it('parametreler dogru gonderilmisse kullanici silinmeli', function (done) {
            request
                .post('/kullanici/kullaniciSil')
                .type('json')
                .send({ yonlendir: '/', silinecekler: [silinecekKullanici1._id] })
                .end(function (err, res) {
                    co(function* () {
                        //Silinen kullanici veritabanindan kontrol ediliyor
                        var c = yield Kullanici.count({ _id: silinecekKullanici1._id }).exec();
                        expect(c === 0).to.be.true;
                        done();
                    }).catch(function (err) {
                        done(err);
                    });
                });
        });

        it('coklu silme islemlerine izin vermeli', function (done) {
            request
                .post('/kullanici/kullaniciSil')
                .type('json')
                .send({ yonlendir: '/', silinecekler: [silinecekKullanici1._id, silinecekKullanici2._id] })
                .expect(302)
                .end(function (err, res) {
                    co(function* () {
                        var kull1 = yield Kullanici.count({ _id: silinecekKullanici1._id }).exec();
                        var kull2 = yield Kullanici.count({ _id: silinecekKullanici2._id }).exec();
                        expect(kull1).to.equal(0);
                        expect(kull2).to.equal(0);
                        done();
                    }).catch(function (err) {
                        done(err);
                    });
                });
        });

        it('silinmek istenen kullanici kendi ise 400 dondurmeli', function (done) {
            co(function* () {
                request
                    .post('/kullanici/kullaniciSil')
                    .type('json')
                    .send({ yonlendir: '/', silinecekler: [admin._id] })
                    .expect(409, done);
            });
        });

        it('kullanici silme islemi gunluge eklenmeli', function (done) {
            co(function* () {

                //Gunluk temizleniyor
                yield Gunluk.remove().exec();

                request
                    .post('/kullanici/kullaniciSil')
                    .type('json')
                    .send({ yonlendir: '/', silinecekler: [silinecekKullanici1._id] })
                    .end(function (err, res) {
                        co(function* () {
                            var c = yield Gunluk.count({
                                tip: GunlukEnum.Tipler.Eylem,
                                aksiyon: GunlukEnum.Aksiyonlar.KullaniciSilme,
                                aktorler: {
                                    $in: [admin._id]
                                }
                            }).exec();

                            expect(c > 0).to.be.true;
                            done();
                        }).catch(function (err) {
                            done(err);
                        });
                    });
            });
        });
    });
});

describe('GET /kullanici/listele', function () {
    var request;

    beforeEach(function (done) {
        request = session(originalApp);

        co(function* () {
            yield Kullanici.remove().exec();
            done();
        });
    });

    afterEach(function (done) {
        co(function* () {
            yield Kullanici.remove().exec();
            done();
        });
    });

    it('admin olarak girildiyse izin vermeli', function (done) {
        co(function* () {
            var admin = yield new Kullanici({
                isim: 'admin',
                sifre: 'admin',
                rol: Roller.Admin
            }).save();

            request
                .post('/giris')
                .type('json')
                .send({ isim: admin.isim, sifre: admin.sifre })
                .end(function (err, res) {
                    request
                        .get('/kullanici/listele')
                        .expect(200)
                        .end(done);
                });
        })
    });

    it('ogretmen olarak girildiyse izin vermeli', function (done) {
        co(function* () {
            var ogretmen = yield new Kullanici({
                isim: 'ogretmen',
                sifre: 'ogretmen',
                rol: Roller.Ogretmen
            }).save();

            request
                .post('/giris')
                .type('json')
                .send({ isim: ogretmen.isim, sifre: ogretmen.sifre })
                .expect(302)
                .end(function (err, res) {
                    request
                        .get('/kullanici/listele')
                        .expect(200)
                        .end(done);
                });

        });
    });

    it('ogrenci olarak girildiyse 403 dondurmeli', function (done) {
        co(function* () {
            var ogrenci = yield new Kullanici({
                isim: 'ogrenci',
                sifre: 'ogrenci',
                rol: Roller.Ogrenci
            }).save();

            request
                .post('/giris')
                .type('json')
                .send({ isim: ogrenci.isim, sifre: ogrenci.sifre })
                .end(function (err, res) {
                    request
                        .get('/kullanici/listele')
                        .expect(403, done);
                });
        });
    });

    describe('yetkilendirme basariyla gecilmisse => ', function () {

        var request, kull;

        beforeEach(function (done) {
            request = session(originalApp);

            co(function* () {
                yield Kullanici.remove().exec();

                kull = yield new Kullanici({
                    isim: 'yetkiliKullanici',
                    sifre: 'yetkiliKullanici',
                    rol: Roller.Admin
                }).save();

                request
                    .post('/giris')
                    .type('json')
                    .send({ isim: kull.isim, sifre: kull.sifre })
                    .expect(302, done);
            });
        });

        afterEach(function (done) {
            co(function* () {
                yield Kullanici.remove().exec();
                done();
            });
        });

        it('tum kullanicilar listele', function (done) {
            co(function* () {
                var user1 = yield new Kullanici({
                    isim: 'user1',
                    sifre: 'user1',
                    rol: Roller.Admin
                }).save();
                var user2 = yield new Kullanici({
                    isim: 'user2',
                    sifre: 'user2',
                    rol: Roller.Veli
                }).save();

                request
                    .get('/kullanici/listele')
                    .expect(200)
                    .end(function (err, res) {
                        co(function* () {
                            var kullaniciSayisi = yield Kullanici.count().exec();
                            var $ = cheerio.load(res.text);
                            var rowCount = $('#kullanicilar-tablosu tbody').find($('tr')).length;
                            expect(rowCount).to.equal(kullaniciSayisi - 1);// -1 cunku kullanici kendini gostermemeli
                            done();
                        }).catch(function (err) {
                            done(err);
                        });
                    });
            });
        });

        it('-contains- filtresi test', function (done) {

            co(function* () {
                var user1 = yield new Kullanici({
                    isim: 'containsFilter',
                    sifre: 'containsFilter',
                    rol: Roller.Admin
                }).save();
                var user2 = yield new Kullanici({
                    isim: 'yanlis',
                    sifre: 'yanlis',
                    rol: Roller.Veli
                }).save();


                //'cont' kelimesini iceren kullanicilar icin istek yollaniyor
                request
                    .get('/kullanici/listele?isim=cont')
                    .expect(200)
                    .end(function (err, res) {
                        co(function* () {
                            var kullSayisi = yield Kullanici.count({ isim: { $regex: 'cont' } }).exec();
                            var $ = cheerio.load(res.text);
                            var rowCount = $('#kullanicilar-tablosu tbody').find($('tr')).length;
                            expect(rowCount).to.equal(kullSayisi);
                            done();
                        }).catch(function (err) {
                            done(err);
                        });
                    });
            });
        });
    });
});

describe('GET /kullanici/veliAta?id=<idParam>', function () {
    var request;

    beforeEach(function (done) {
        co(function* () {
            request = session(originalApp);
            yield Kullanici.remove();
            done();
        }).catch(function (err) {
            done(err);
        });
    });

    it('kullanici ogrenciyse 403 dondurur', function (done) {
        co(function* () {
            var ogrenci = new Kullanici({
                isim: 'ogrenci',
                sifre: 'ogrenci',
                rol: Roller.Ogrenci
            });

            var result = yield ogrenci.save();

            request
                .post('/giris')
                .type('json')
                .send({ isim: ogrenci.isim, sifre: ogrenci.sifre })
                .end(function (err, res) {
                    request
                        .get('/kullanici/veliAta')
                        .expect(403, done);
                });
        }).catch(function (err) {
            done(err);
        });
    });

    it('kullanici veliyse 403 dondurur', function (done) {
        co(function* () {
            var veli = new Kullanici({
                isim: 'veli',
                sifre: 'veli',
                rol: Roller.Veli
            });

            yield veli.save();

            request
                .post('/giris')
                .type('json')
                .send({ isim: veli.isim, sifre: veli.sifre })
                .end(function (err, res) {
                    request
                        .get('/kullanici/veliAta')
                        .expect(403, done);
                });

        }).catch(function (err) {
            done(err);
        });
    });

    it('kullanici admin ise ve belirtilen id ogrenci degil ise 400 dondurur', function (done) {
        co(function* () {
            var ogrenci = new Kullanici({
                isim: 'ogrenci',
                sifre: 'ogrenci',
                rol: Roller.Ogrenci
            });

            var admin = new Kullanici({
                isim: 'admin',
                sifre: 'admin',
                rol: Roller.Admin
            });

            var veli = new Kullanici({
                isim: 'veli',
                sifre: 'veli',
                rol: Roller.Veli
            });

            var ogretmen = new Kullanici({
                isim: 'ogretmen',
                sifre: 'ogretmen',
                rol: Roller.Ogretmen
            });

            var admin2 = new Kullanici({
                isim: 'admin2',
                sifre: 'admin2',
                rol: Roller.Admin
            });

            yield ogrenci.save();
            yield admin.save();
            yield veli.save();

            request
                .post('/giris')
                .type('json')
                .send({ isim: admin.isim, sifre: admin.sifre })
                .end(function (err, res) {

                    var urlBase = '/kullanici/veliAta?id=';
                    var veliRequest = urlBase + veli._id;
                    async.series([
                        function (cb) { request.get('/kullanici/veliAta?id=' + veli._id).expect(400, cb); },
                        function (cb) { request.get('/kullanici/veliAta?id=' + ogretmen._id).expect(400, cb); },
                        function (cb) { request.get('/kullanici/veliAta?id=' + admin2._id).expect(400, cb); }
                    ], done);
                });

        }).catch(function (err) {
            done(err);
        });
    });



    it('admin olarak giris yapilmis ve belirtilen idye karsilik kgelen kullanici mevcut degilse 400 donrurur', function (done) {
        co(function* () {
            var admin = new Kullanici({
                isim: 'admin',
                sifre: 'admin',
                rol: Roller.Admin
            });

            yield admin.save();

            request
                .post('/giris')
                .type('json')
                .send({ isim: admin.isim, sifre: admin.sifre })
                .end(function (err, res) {
                    if (err) done(err);

                    request
                        .get('/kullanici/veliAta?id=59ca11042a0ccf52f8190b11')
                        .expect(400, done);
                });
        });
    });

    it('admin olarak giris yapilmis ama id belirtilmemisse 400 dondurur', function (done) {
        co(function* () {
            var admin = new Kullanici({
                isim: 'admin',
                sifre: 'admin',
                rol: Roller.Admin
            });

            yield admin.save();

            request
                .post('/giris')
                .type('json')
                .send({ isim: admin.isim, sifre: admin.sifre })
                .end(function (err, res) {
                    if (err) done(err);

                    request
                        .get('/kullanici/veliAta')
                        .expect(400, done);
                });
        });
    });


    it('kullanici admin ise ve ogrenci idsi belirtilmis ise 200 dondurur', function (done) {
        co(function* () {

            var ogrenci = new Kullanici({
                isim: 'ogrenci',
                sifre: 'veli',
                rol: Roller.Ogrenci
            });
            var admin = new Kullanici({
                isim: 'admin',
                sifre: 'admin',
                rol: Roller.Admin
            });

            yield ogrenci.save();
            yield admin.save();

            request
                .post('/giris')
                .type('json')
                .send({ isim: admin.isim, sifre: admin.sifre })
                .end(function (err, res) {
                    request
                        .get('/kullanici/veliAta?id=' + ogrenci._id)
                        .expect(200, done);
                });
        });
    });

    it('kullanici admin ise ve sonuc FormSonuc.Error olarak belirtilmisse 500 dondurur' , function(done){
        co(function*(){
            var ogrenci = new Kullanici({
                isim : 'ogrenci' , 
                sifre : 'ogrenci' ,
                rol : Roller.Ogrenci
            });
            var admin = new Kullanici({
                isim : 'admin' , 
                sifre : 'admin' , 
                rol : Roller.Admin
            });

            ogrenci = yield ogrenci.save();
            yield admin.save();

            request
                .post('/giris')
                .type('json')
                .send({ isim : admin.isim , sifre : admin.sifre })
                .end(function(err , res){
                    if(err) done(err);
                    var formattedUrl = url.format({
                        pathname : '/kullanici/veliAta' ,
                        query : {
                            id : ogrenci._id.toString() ,
                            sonuc : Enums.FormSonuc.Error ,
                            mesaj : 'Hata'
                        }
                    });

                    console.log('Formatted url => ' + formattedUrl);
                    request
                        .get(formattedUrl)
                        .expect(500 , done);
                });
        });
    });

    it('kullanici admin ise ve sonuc FormSonuc.Success olarak belirtilmisse 200 dondurur ' , function(done){
        co(function*(){
            var ogrenci = new Kullanici({
                isim : 'ogrenci' ,
                sifre:  'ogrenci' , 
                rol : Roller.Ogrenci
            });
            var admin = new Kullanici({
                isim : 'admin' , 
                sifre : 'admin' ,
                rol : Roller.Admin
            });

            yield ogrenci.save();
            yield admin.save();

            request 
                .post('/giris')
                .type('json')
                .send({ isim : admin.isim , sifre : admin.sifre })
                .end(function(err , res){
                    request
                        .get(url.format({
                            pathname : '/kullanici/veliAta' ,
                            query : {
                                id : ogrenci._id.toString() ,
                                sonuc : Enums.FormSonuc.Success ,
                                mesaj : 'Basarili'
                            }
                        }))
                        .expect(200 , done);
                });

        }).catch(done);
    });

});

describe('POST /kullanici/veliAta', function () {

    var request;

    beforeEach(function (done) {
        request = session(originalApp);

        co(function* () {
            yield Kullanici.remove().exec();
            done();
        });
    });

    it('Ogrenci olarak giris yapilmissa 403 donduruluyor', function (done) {
        co(function* () {
            var ogrenci = new Kullanici({
                isim: 'ogrenci',
                sifre: 'ogrenci',
                rol: Roller.Ogrenci
            });

            yield ogrenci.save();

            request
                .get('/giris')
                .type('json')
                .send({ isim: ogrenci.isim, sifre: ogrenci.sifre })
                .end(function (err, res) {
                    request
                        .post('/kullanici/veliAta')
                        .type('json')
                        .expect(403, done);
                });

        });
    });

    it('Veli olarak giris yapilmissa 403 donduruluyor', function (done) {
        co(function* () {
            var veli = new Kullanici({
                isim: 'veli',
                sifre: 'veli',
                rol: Roller.Veli
            });

            yield veli.save();

            request
                .get('/giris')
                .type('json')
                .send({ isim: veli.isim, sifre: veli.sifre })
                .end(function (err, res) {
                    request
                        .post('/kullanici/veliAta')
                        .type('json')
                        .expect(403, done);
                });
        });
    });

    describe('Admin olarak giris yapilmissa', function () {

        var request, admin;

        beforeEach(function (done) {
            request = session(originalApp);

            co(function* () {

                yield Kullanici.remove().exec();
                yield Gunluk.remove().exec();
                yield Ebeveyn.remove().exec();

                admin = new Kullanici({
                    isim: 'admin',
                    sifre: 'admin',
                    rol: Roller.Admin
                });

                yield admin.save();

                request
                    .post('/giris')
                    .type('json')
                    .send({ isim: admin.isim, sifre: admin.sifre })
                    .end(function (err, res) {
                        done();
                    });

            });
        });

        it('ogrenciId bos gonderilirse 400 dondurur', function (done) {

            request
                .post('/kullanici/veliAta')
                .type('json')
                .send({ veliId: '59ca11042a0ccf52f8190b11' })
                .expect(400, done);

        });

        it('veliId bos gonderilirse 400 dondurur', function (done) {
            request
                .post('/kullanici/veliAta')
                .type('json')
                .send({ ogrenciId: '59ca11042a0ccf52f8190b11' })
                .expect(400, done);
        });

        it('Model dogru fakat ogrenciIdye karsilik gelen kullanici yoksa 400 dondurur', function (done) {
            co(function* () {
                var veli = new Kullanici({
                    isim: 'veli',
                    sifre: 'veli',
                    rol: Roller.Veli
                });
                yield veli.save();
                request
                    .post('/kullanici/veliAta')
                    .type('json')
                    .send({ ogrenciId: '59ca11042a0ccf52f8190b11', veliId: veli._id })
                    .expect(400, done);
            });
        });

        it('Model dogru fakat veliIdye karsilik gelen kullanici yoksa 400 dondurur', function (done) {
            co(function* () {
                var ogrenci = new Kullanici({
                    isim: 'ogrenci',
                    sifre: 'ogrenci',
                    rol: Roller.Veli
                });

                yield ogrenci.save();

                request
                    .post('/kullanici/veliAta')
                    .type('json')
                    .send({ ogrenciId: ogrenci._id, veliId: '59ca11042a0ccf52f8190b11' })
                    .expect(400, done);
            });
        });

        it('Model dogru fakat veliIdye denk gelen kullanici veli degilse 400 dondurur', function (done) {
            co(function* () {
                //Veli olmayan kullanici
                var ogretmen = new Kullanici({
                    isim: 'ogretmen',
                    sifre: 'ogretmen',
                    rol: Roller.Ogretmen
                });

                var ogrenci = new Kullanici({
                    isim: 'ogrenci',
                    sifre: 'ogrenci',
                    rol: Roller.Ogrenci
                });

                yield ogretmen.save();
                yield ogrenci.save();

                request
                    .post('/kullanici/veliAta')
                    .type('json')
                    .send({ ogrenciId: ogrenci._id, veliId: ogretmen._id })
                    .expect(400, done);
            });
        });

        it('Model dogru fakat ogrenciIdye denk gelen kullanici ogrenci degilse 400 dondurur', function (done) {
            co(function* () {
                //Ogrenci olmayan kullanici
                var ogretmen = new Kullanici({
                    isim: 'ogretmen',
                    sifre: 'ogretmen',
                    rol: Roller.Ogretmen
                });

                var veli = new Kullanici({
                    isim: 'veli',
                    sifre: 'veli',
                    rol: Roller.Veli
                });

                yield ogretmen.save();
                yield veli.save();

                request
                    .post('/kullanici/veliAta')
                    .type('json')
                    .send({ ogrenciId: ogretmen._id, veliId: veli._id })
                    .expect(400, done);

            });
        });

        it('Model dogru belirtilmis ve idler dogru olarak eslesiyorsa 200 dondurur', function (done) {
            co(function* () {
                var ogrenci = new Kullanici({
                    isim: 'ogrenci',
                    sifre: 'ogrenci',
                    rol: Roller.Ogrenci
                });
                var veli = new Kullanci({
                    isim: 'veli',
                    sifre: 'veli',
                    rol: Roller.Veli
                });

                yield ogrenci.save();
                yield veli.save();

                request
                    .post('/kullanici/veliAta')
                    .type('json')
                    .send({ ogrenciId: ogrenci._id, veliId: veli._id })
                    .expect(200)
                    .end(function (err, res) {
                        co(function* () {
                            var res = yield Ebeveyn.findOne({ ogrenciId: ogrenci._id, veliId: veli._id }).exec();

                            expect(res).not.to.be.null;
                            done();
                        });
                    });
            });
        });

        it('Ebeveyn atandiysa sistem gunlugune yazmali', function (done) {
            co(function* () {
                var ogrenci = new Kullanici({
                    isim: 'ogrenci',
                    sifre: 'ogrenci',
                    rol: Roller.Ogrenci
                });

                var veli = new Kullanici({
                    isim: 'veli',
                    sifre: 'veli',
                    rol: Roller.Veli
                });

                yield ogrenci.save();
                yield veli.save();

                request
                    .post('/kullanici/veliAta')
                    .type('json')
                    .send({ ogrenciId: ogrenci._id, veliId: veli._id })
                    .expect(200)
                    .end(function (err, res) {
                        co(function* () {
                            var c = yield Gunluk.count().exec();
                            expect(c > 0).to.be.true;
                            var log = yield Gunluk.find({
                                aksiyon: GunlukEnum.Aksiyonlar.VeliAta,
                                tip: GunlukEnum.Tipler.Eylem,
                                aktorler: {
                                    $in: [admin._id]
                                },
                                veri: {
                                    ogrenciId: ogrenci._id,
                                    veliId: veli._id
                                }
                            });
                            done();
                        });
                    });
            });
        });
    });
});