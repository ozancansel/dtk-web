'use strict'
var session = require('supertest-session');
var expect = require('chai').expect;
var app = require('../app');
var co = require('co');
var Kullanici = require('../model/kullanici');
var Gunluk = require('../model/sistemgunlugu');
var GunlukEnum = require('../model/enum/sistemgunluguenum');
var Yetkiler = require('../model/yetkiler');

describe('GET /', function () {
    var request;

    beforeEach(function () {
        request = session(app);
    });

    var kullanicilariTemizle = function* () {
        yield Kullanici.remove({});
    };

    it('kullanici giris yapmamissa giris sayfasina yonlendirmeli', function (done) {
        request.get('/')
            .expect(302)
            .end(function (err, res) {
                if (err) throw err;
                expect(res.headers['location']).to.equal('/giris');
                done();
            });
    });

    it('kullanci giris yapmissa anasayfaya yonlendirmeli', function (done) {
        co(function* () {
            yield kullanicilariTemizle();
            var kull = new Kullanici({
                isim: 'isim',
                sifre: 'sifre',
                rol: Yetkiler.Admin
            });

            var k = yield kull.save();

            request
                .post('/giris')
                .type('json')
                .send({ isim: 'isim', sifre: 'sifre' })
                .then(function () {
                    request
                        .get('/')
                        .expect(200, done);
                });
        });
    });
});

describe('GET /giris', function () {
    var request;

    beforeEach(function () {
        request = session(app);
    });

    it('200 dondurmeli', function (done) {
        request
            .get('/giris')
            .expect(200)
            .end(done);
    });

});

describe('POST /giris', function () {
    var request, kull = {
        isim: 'isim',
        sifre: 'sifre',
        rol: Yetkiler.Admin
    };

    beforeEach(function () {
        request = session(app);
    });

    before(function (done) {
        co(function* () {
            yield Kullanici.remove({});
            var yeniKull = new Kullanici(kull);
            kull._id = yeniKull._id;
            yield yeniKull.save();
            done();
        });
    });

    var hataliDenemeler = [
        { body: { isim: kull.isim, sifre: kull.sifre + "1"/*Hatali sifre icin*/ }, aciklama: "isim dogru sifre yanlissa " },
        { body: { isim: kull.isim + "1"/*Hatali isim icin*/, sifre: kull.sifre }, aciklama: "isim yanlis sifre dogruysa " },
        { body: { isim: kull.isim + "1", sifre: kull.sifre + "1" }, aciklama: "isim ve sifre yanlissa" }
    ];

    hataliDenemeler.forEach(function (deneme) {
        it(deneme.aciklama + " giris sayfasi tekrar acilmali ve 401 dondurmeli", function (done) {
            request
                .post('/giris')
                .type('json')
                .send(deneme.body)
                .expect(401, done);
        });
    });

    it('kullanici bilgileri dogruysa 302 dondurmeli ve anasayfaya yonlendirmeli', function (done) {
        request
            .post('/giris')
            .type('json')
            .send(kull)
            .expect(302)
            .end(function (err, res) {
                if (err) throw err;

                expect(res.headers['location']).to.equal('/');
                done();
            });
    });

    it('kullanici login olduysa sistem gunluguna yazilmali', function (done) {
        co(function* () {
            //Gunluk kayitlari temizleniyor
            yield Gunluk.remove({});

            request
                .post('/giris')
                .type('json')
                .send(kull)
                .expect(302)
                .end(function (err, res) {
                    co(function* () {
                        var c = yield Gunluk.count().exec();
                        expect(c > 0).to.be.true;
                        var kayit = yield Gunluk.findOne({
                            aksiyon: GunlukEnum.Aksiyonlar.Giris,
                            tip: GunlukEnum.Tipler.Eylem,
                            aktorler: [kull._id]
                        });
                        expect(kayit).to.not.be.null;
                        done();
                    }).catch(function (err) {
                        done(err);
                    });
                });
        });
    });
});

describe('GET /cikis', function () {
    var request, kull = {
        isim: 'isim',
        sifre: 'sifre',
        rol: Yetkiler.Veli
    };

    beforeEach(function (done) {
        request = session(app);

        request
            .post('/giris')
            .type('json')
            .send(kull)
            .expect(302, done);
    });

    before(function (done) {
        co(function* () {
            //Kullanicilar temizleniyor
            yield Kullanici.remove({});
            var k = yield new Kullanici(kull).save();
            kull._id = k._id;
            done();
        });
    });

    it('cikis yapilinca /giris sayfasina yonlendirilmeli', function (done) {

        request
            .get('/cikis')
            .expect(302)
            .end(function (err, res) {
                expect(res.headers['location']).to.equal('/giris');
                done();
            });
    });

    it('cikis yapilirsa sistem gunlugune yazilmali', function (done) {
        co(function* () {
            //Gunluk temizleniyor
            yield Gunluk.remove({});

            request
                .get('/cikis')
                .end(function (err, res) {
                    co(function* () {
                         var c = yield Gunluk.count().exec();
                         expect(c > 0 ).to.be.true;
                         var kayit = yield Gunluk.find({ 
                             aksiyon : GunlukEnum.Aksiyonlar.Cikis ,
                             tip : GunlukEnum.Tipler.Eylem ,
                             aktorler : kull._id
                                         }).exec();
                        expect(kayit).to.not.be.null;
                        done();
                    }).catch(function(err){
                        done(err);
                    });
                });
        });
    });
});