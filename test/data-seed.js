var mongoose = require('mongoose');
var Kullanici = require('../model/kullanici');
var Yetkiler = require('../model/yetkiler');

module.exports = function(){
    var dayInMillis = 24*3600*1000;
    var admin = new Kullanici({
        isim : "admin",
        email : "admin@dtk.com" ,
        telefon : "",
        sifre : "password",
        rol : Yetkiler.Admin,
        kayitTarihi : Date.now() - 365 * dayInMillis //365 gun once
    });
    var ozan = new Kullanici({
        _id : new mongoose.Types.ObjectId,
        isim : "OzanCansel",
        email : "ozancansel@gmail.com",
        telefon : "+905340628572",
        sifre : "ozansifre",
        rol : Yetkiler.Ogretmen ,
        kayitTarihi : Date.now() - 60 * dayInMillis //60 gun once
    });
    var ahmet = new Kullanici({
        _id : new mongoose.Types.ObjectId,
        isim : "AhmetKanat",
        telefon : "+905330768573",
        sifre : "ahmetsifre",
        rol : Yetkiler.Ogrenci,
        kayitTarihi : Date.now() - 59 * dayInMillis //59 gun once
    });
    var ece = new Kullanici({
        _id : new mongoose.Types.ObjectId,
        isim : "EceErmin",
        telefon : "+905540869845",
        sifre : "ahmetsifre" , 
        rol : Yetkiler.Ogrenci ,
        kayitTarihi : Date.now() - 58 * dayInMillis //58 gun once
    });
};