var winston = require('winston');
var Gunluk = require('./model/sistemgunlugu');
require('winston-daily-rotate-file');
var dailyTransport = new (winston.transports.DailyRotateFile)({
    filename : '.log' ,
    datePattern : 'yyyy-MM-dd',
    prepend : true ,
    level: process.env.ENV === 'development' ? 'debug' : 'info' ,
});

var logger = new winston.Logger({
    transports : [
        dailyTransport
    ]
});

module.exports.info = function(msg , data){
    console.log('Error Msg => ' + msg + ' Data => ' + data);    
    logger.info('info' , msg , data);
}

module.exports.error = function(msg , data){
    console.log('Error Msg => ' + msg + ' Data => ' + data);
    logger.info('error' , msg , data);
}

module.exports.dbInfoLog = function*(msg , data){
    logger.info('dblog-info' , msg , data);

    var kayit = new Gunluk(data);
    yield kayit.save();
}

module.exports.dbErrorLog = function*(msg , stacktrace , data){
    logger.error('dblog-error' , msg + " StackTrace : " + stacktrace , data);

    var kayit = new Gunluk(data);
    yield kayit.save();
}

module.exports.winstongLogger = function(){
    return logger;
}