/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var Kullanici = require('../model/kullanici');
var Yetkiler = require('../model/yetkiler');
var co = require('co');

module.exports.olustur = function* () {
    var admin = new Kullanici({
        isim: "admin",
        sifre: "password",
        rol: Yetkiler.Admin
    });

    var user = yield Kullanici.findOne({isim: admin.isim}).exec();
    var result;
    
    if (user === null) {
        result = yield Kullanici.findOneAndUpdate(
                {//Arama ayarlari
                    isim: admin.isim
                },
                {//$$set 
                    sifre: admin.sifre,
                    rol: admin.rol
                },
                {//Sorgu ayarlari
                    upsert: true,
                    new : true,
                    setDefaultsOnInsert: true
                });
            }

    return result;
};

module.exports.initTestDb = function(){
    
}

module.exports.init = function(){
  co(function *(){
     var res = yield module.exports.olustur(); 
  });  
};