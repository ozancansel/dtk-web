/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var Kullanici = require('../model/kullanici');

module.exports.ekle = function* (kull, islemiGerceklestiren) {
    //Islemi gerceklestiren admin degilse hata firlatiliyor
    if (islemiGerceklestiren.yetki !== Kullanici.Yetkiler.Admin) {
        throw "Yetkilendirme hatasi";
    }

    var result = yield Kullanici.findOneAndUpdate(
            {//Arama ayarlari
                isim: kull.isim
            },
            {//$$set 
                sifre: kull.sifre,
                yetki: kull.yetki
            },
            {//Sorgu ayarlari
                upsert: true,
                new : true,
                setDefaultsOnInsert: true
            });

    return result;
};
